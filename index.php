<!DOCTYPE html>
<html>
    <head>
        <title>Tree menu</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="public/css/tree.css">
        <script src="public/js/utils.js"></script>
        <script src="public/js/tree.js"></script>
    </head>
    <body>
        <div class="container">
            <h1>Tree menu</h1>
            <p class="intro">
                To interact with the menu: left mouse button to expand/collapse, right mouse button to open context menu.
            </p>
            <div id="tree-controls" class="hidden">
                <button type="button" class="expand submit">Expand all</button>
                <button type="button" class="collapse submit">Collapse all</button>
            </div>
            <ul id="tree" class="tree"></ul>

            <!-- Popups -->
            <div id="item_context_menu_popup" class="popup context-menu">
                <ul>
                    <li><a href="#" class="add_item">Add new item</a></li>
                    <li><a href="#" class="edit_item">Edit</a></li>
                    <li><a href="#" class="delete_item">Delete</a></li>
                </ul>
            </div>
            <div id="add_popup" class="popup add">
                <h3>Add new item</h3>
                <ul class="errors"></ul>
                <form id="form_add">
                    <div class="form-row">
                        <label>Title <input type="text" name="title" class="title" /></label>
                    </div>
                    <div class="controls clearfix">
                        <button type="button" class="action cancel">Cancel</button>
                        <button type="submit" class="action submit float-right">Add</button>
                    </div>
                </form>
            </div>
            <div id="edit_popup" class="popup edit">
                <h3>Edit item</h3>
                <ul class="errors"></ul>
                <form id="form_add">
                    <div class="form-row">
                        <label>Title <input type="text" name="title" class="title" /></label>
                    </div>
                    <div class="controls clearfix">
                        <button type="button" class="action cancel">Cancel</button>
                        <button type="submit" class="action submit float-right">Edit</button>
                    </div>
                </form>
            </div>
            <div id="delete_popup" class="popup delete">
                <h3>Delete <span class="placeholder">%item%</span>?</h3>
                <ul class="errors"></ul>
                <div class="controls clearfix">
                    <button type="button" class="action cancel">Cancel</button>
                    <button type="button" class="action submit delete float-right">Delete</button>
                </div>
            </div>
            <div id="error_popup" class="popup error">
                <p class="error"></p>
                <div class="controls clearfix">
                    <button type="button" class="action cancel float-right">Close</button>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        <?php
        // Compute api uri
        $uri = filter_input(INPUT_SERVER, 'REQUEST_SCHEME')
            .'://'
            .filter_input(INPUT_SERVER, 'HTTP_HOST')
            .str_replace('index.php', '', filter_input(INPUT_SERVER, 'REQUEST_URI'))
            .'api/tree';
        ?>
        window.addEventListener('load', function() {
            Tree.init('<?php echo $uri; ?>');
        });
        </script>
    </body>
</html>

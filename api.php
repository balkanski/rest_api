<?php

use Api\ApiFactory;
use Exception\ApiException;
use Http\Http;
use Http\Request;
use Http\Response;

require 'Loader/Autoloader.php';
Autoloader::register();

try {
    require 'config/database.php';
    $pdo = new PDO($dsn, $db_user, $db_pass, $pdo_opt);

    $request = new Request();
    $api = ApiFactory::createApi($request, $pdo);
    $response = $api->process();

    Http::send($response);
} catch (Exception $e) {
    if ($e instanceof ApiException) {
        $statusCode = $e->getStatusCode();
    } else {
        $statusCode = Http::STATUS_ERROR;
    }

    Http::send(new Response(json_encode(array('error' => $e->getMessage())), $statusCode));
}

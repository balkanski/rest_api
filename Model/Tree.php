<?php

namespace Model;

use PDO;

class Tree
{
    /**
     * The root node.
     *
     * @var Node
     */
    public $root;

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * The whole resource uri without the resource id.
     *
     * @var string
     */
    protected $resourceUri;

    /**
     * @param PDO    $pdo
     * @param string $resourceUri
     */
    public function __construct(PDO $pdo, $resourceUri)
    {
        $this->pdo = $pdo;
        $this->resourceUri = $resourceUri;
    }

    /**
     * Selects a node by resource id.
     *
     * @param int $id
     *
     * @return Node
     */
    public function getNode($id = null)
    {
        $this->build();
        $node = $id ?
            $this->searchNode($this->getRoot(), $id) :
            $this->getRoot();
        if ($node) {
            $node->loadResourceUris($this->getResourceUri());
        }

        return $node;
    }

    /**
     * @return Node
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @return PDO
     */
    protected function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @return string
     */
    protected function getResourceUri()
    {
        return $this->resourceUri;
    }

    /**
     * Builds the whole tree.
     */
    protected function build()
    {
        $nodes = array();

        $stmt = $this->getPdo()->query('SELECT * FROM tree ORDER BY parent_id, title');
        while ($row = $stmt->fetch()) {
            $node = new Node($row['id'], $row['title'], $row['parent_id'], $this->getPdo());
            $nodes[$node->getId()] = $node;

            if ($node->getParentId()) {
                $nodes[$node->getParentId()]->addChild($node);
            }
        }

        $this->root = array_shift($nodes);
    }

    /**
     * Create a new node as a child of $parentNode.
     *
     * @param Node  $parentNode
     * @param array $data       The data for the new node
     *
     * @return Node
     */
    public function createNode(Node $parentNode, $data)
    {
        $node = new Node(null, $data['title'], $parentNode->getId(), $this->getPdo());
        $node->save();
        $node->setResourceUri($this->getResourceUri().'/'.$node->getId());

        return $node;
    }

    /**
     * Update a node with new data.
     *
     * @param Node  $node
     * @param array $data
     *
     * @return Node
     */
    public function updateNode(Node $node, $data)
    {
        $node->setTitle($data['title']);
        $node->save();

        return $node;
    }

    /**
     * Delete node.
     *
     * @param Node $node
     */
    public function deleteNode(Node $node)
    {
        $node->delete();
    }

    /**
     * Search for node by id in a given node(tree).
     *
     * @param Node $root
     * @param int  $id
     *
     * @return Node if found or NULL
     */
    protected function searchNode(Node $root, $id)
    {
        $result = $this->search($root, $id);

        return empty($result) ?
            null :
            $result[0];
    }

    /**
     * Recursive method to find a node by id in a tree.
     *
     * @param Node $node
     * @param int  $id
     *
     * @return array Contains the found node or is empty
     */
    protected function search(Node $node, $id)
    {
        if ($node->getId() == $id) {
            return array($node);
        } elseif ($node->isLeaf()) {
            return array();
        } else {
            $result = array();
            foreach ($node->getChildren() as $child) {
                $result = array_merge($result, $this->search($child, $id));
            }

            return $result;
        }
    }
}

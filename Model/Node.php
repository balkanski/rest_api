<?php

namespace Model;

use Api\Resource;
use Api\ResourceUriTrait;
use PDO;

/**
 * @implements Resource
 */
class Node implements Resource
{
    use ResourceUriTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var int
     */
    protected $parentId;

    /**
     * @var array
     */
    protected $children;

    /**
     * @var string
     */
    protected $resourceUrl;

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @param int    $id
     * @param string $title
     * @param int    $parentId
     * @param PDO    $pdo
     */
    public function __construct($id, $title, $parentId, PDO $pdo)
    {
        $this->id = $id;
        $this->title = $title;
        $this->parentId = $parentId;
        $this->children = array();
        $this->pdo = $pdo;
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return $this->getParentId() ?
            false :
            true;
    }

    /**
     * @return bool
     */
    public function isLeaf()
    {
        return count($this->children) > 0 ?
            false :
            true;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Node $child
     */
    public function addChild(Node $child)
    {
        $this->children[] = $child;
    }

    /**
     * @return PDO
     */
    protected function getPdo()
    {
        return $this->pdo;
    }

    /**
     * Converts Node properties to json data string.
     *
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * Load current and child nodes with the proper resource uri.
     *
     * @param string $uri
     */
    public function loadResourceUris($uri)
    {
        $this->setResourceUri($uri.'/'.$this->getId());

        if (!$this->isLeaf()) {
            foreach ($this->getChildren() as $child) {
                $child->loadResourceUris($uri);
            }
        }
    }

    /**
     * Save node into the DB.
     *
     * @return Node
     */
    public function save()
    {
        if ($this->isNew()) {
            $stmt = $this->getPdo()->prepare('INSERT INTO tree (title, parent_id) VALUES (?, ?)');
            $stmt->execute([$this->getTitle(), $this->getParentId()]);
            $this->setId($this->getPdo()->lastInsertId());
        } else {
            $stmt = $this->getPdo()->prepare('UPDATE tree SET title = ? WHERE id = ?');
            $stmt->execute([$this->getTitle(), $this->getId()]);
        }

        return $this;
    }

    /**
     * Delete node from DB.
     */
    public function delete()
    {
        $stmt = $this->getPdo()->prepare('DELETE FROM tree WHERE id = ?');
        $stmt->execute([$this->getId()]);
    }

    /**
     * Checks if node is new or is already created and stored in the DB.
     *
     * @return bool
     */
    protected function isNew()
    {
        return $this->getId() ?
            false :
            true;
    }

    /**
     * Gets all node properties and prepares them for json formatting.
     *
     * @return array
     */
    protected function toArray()
    {
        $children = array();

        foreach ($this->getChildren() as $child) {
            $children[] = $child->toArray();
        }

        return array(
            'id' => $this->getId(),
            'title' => htmlentities($this->getTitle()),
            'parentId' => $this->getParentId(),
            'children' => $children,
            'uri' => $this->getResourceUri(),
        );
    }
}

<?php

namespace Http;

class Http
{
    const STATUS_OK = '200 OK';
    const STATUS_CREATED = '201 Created';
    const STATUS_NO_CONTENT = '204 No Content';
    const STATUS_FORBIDDEN = '403 Forbidden';
    const STATUS_NOT_FOUND = '404 Not Found';
    const STATUS_METHOD_NOT_ALLOWED = '405 Method Not Allowed';
    const STATUS_UNPROCESSABLE_ENTITY = '422 Unprocessable Entity';
    const STATUS_ERROR = '500 Internal Server Error';

    /**
     * Sends HTTP response.
     *
     * @param Response $response The object storing the data for the HTTP response
     */
    public static function send(Response $response)
    {
        // Send this in case of status code 201 Created
        if ($response->getLocation()) {
            header('Location: '.$response->getLocation());
            exit();
        }

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
        header('Content-Type: application/json; charset=utf-8');
        header('HTTP/1.1 '.$response->getStatus());

        echo $response->getData() ?
            $response->getData() :
            '';
    }
}

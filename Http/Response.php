<?php

namespace Http;

class Response
{
    /**
     * Response status code, e.g. 200 OK.
     *
     * @var string
     */
    protected $status;

    /**
     * Responce body in JSON format.
     *
     * @var string
     */
    protected $data;

    /**
     * A complete uri meant to be used with the Location header when needed.
     *
     * @var string
     */
    protected $location;

    /**
     * @param array  $data
     * @param string $status
     * @param string $location
     */
    public function __construct($data = null, $status = null, $location = null)
    {
        $this->data = $data;
        $this->status = $status;
        $this->location = $location;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
}

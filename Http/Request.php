<?php

namespace Http;

use Exception\ApiException;

class Request
{
    /**
     * Request parameters.
     *
     * @var array
     */
    public $params;

    /**
     * Name of the api resource, e.g. tree, books, notes, customers.
     *
     * @var string
     */
    public $resourceName;

    /**
     * Id of the api resource.
     *
     * @var int
     */
    public $resourceId;

    /**
     * The whole resource uri without the resource id.
     *
     * @var string
     */
    public $resourceUri;

    /**
     * The HTTP request method.
     *
     * @var string
     */
    public $method;

    /**
     * The post data sent via POST or PUT.
     *
     * @var array
     */
    public $data;

    /**
     * @throws ApiException
     */
    public function __construct()
    {
        $this->computeResourceProperties();
        $this->computeMethod();
    }

    /**
     * Compute resource name, id, uri and query params.
     */
    protected function computeResourceProperties()
    {
        // Extract resource name and resource id values
        $apiRoute = filter_input(INPUT_GET, 'apiRoute');
        $apiRouteParts = explode('/', $apiRoute);
        $this->resourceName = $apiRouteParts[0];
        $this->resourceId = @$apiRouteParts[1];

        // Compute full resource uri without the resource id
        $uri = filter_input(INPUT_SERVER, 'REQUEST_URI');
        $uriParts = explode('?', $uri);
        $this->resourceUri =
            filter_input(INPUT_SERVER, 'REQUEST_SCHEME')
                .'://'
                .filter_input(INPUT_SERVER, 'HTTP_HOST')
                .rtrim(preg_replace('!'.$this->resourceId.'$!', '', $uriParts[0]), '/');

        // Store GET query params
        parse_str(@$uriParts[1], $this->params);
    }

    /**
     * Compute HTTP method and store POST/PUT data.
     *
     * @throws ApiException
     */
    protected function computeMethod()
    {
        $this->method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
        switch ($this->method) {
            case 'POST':
                $this->data = json_decode(file_get_contents('php://input'), true);

                // Handle HTTP tunneling(masking PUT/DELETE inside POST)
                if ($methodOverride = filter_input(INPUT_SERVER, 'HTTP_X_HTTP_METHOD_OVERRIDE')) {
                    switch ($methodOverride) {
                        case 'PUT':
                            $this->method = 'PUT';
                            break;

                        case 'DELETE':
                            $this->method = 'DELETE';
                            break;

                        default:
                            throw new ApiException('Unexpected Header X-HTTP-Method-Override', Http::STATUS_METHOD_NOT_ALLOWED);
                    }
                }
                break;

            case 'PUT':
                $this->data = json_decode(file_get_contents('php://input'), true);
                break;
        }
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return string
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * @return string
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * @return string
     */
    public function getResourceUri()
    {
        return $this->resourceUri;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}

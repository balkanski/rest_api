/**
 * Check if a CSS class is set.
 * @param String className
 * @returns {Boolean}
 */
Node.prototype.hasClass = function (className) {
    if (this.classList) {
        return this.classList.contains(className);
    } else {
        return (-1 < this.className.indexOf(className));
    }
};

/**
 * Add a CSS class.
 * @param String className
 * @returns {Node.prototype}
 */
Node.prototype.addClass = function (className) {
    if (this.classList) {
        this.classList.add(className);
    } else if (!this.hasClass(className)) {
        var classes = this.className.split(" ");
        classes.push(className);
        this.className = classes.join(" ");
    }
    return this;
};

/**
 * Remove a CSS class.
 * @param String className
 * @returns {Node.prototype}
 */
Node.prototype.removeClass = function (className) {
    if (this.classList) {
        this.classList.remove(className);
    } else {
        var classes = this.className.split(" ");
        classes.splice(classes.indexOf(className), 1);
        this.className = classes.join(" ");
    }
    return this;
};

/**
 * Wrapper function
 * @param String id
 * @returns {Element}
 */
function getById(id) {
    return document.getElementById(id);
};

/**
 * Decodes html entities
 * @param String html
 * @returns String
 */
function decodeHtml(html) {
    var text = document.createElement("textarea");
    text.innerHTML = html;

    return text.value;
}

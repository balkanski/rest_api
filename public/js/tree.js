////////////////////////////////////
////////// Tree
////////////////////////////////////
/**
 * Load, render and operate with the tree.
 */
var Tree = {
    containerId: 'tree',
    currentNode: null,
    validation: {
        titleLength: 50,
        titleEmptyError: 'Value should not be empty.',
        titleTooLongError: 'Value should be less than 50 characters.'
    },

    /**
     * Load the tree in DOM.
     */
    init: function(resourceUri) {
        this.resourceUri = resourceUri;

        // Load the tree in DOM.
        this.doGet(this.resourceUri, this.loadTree);

        // Init expand/collapse controls.
        this.initControls();

        // Init context menu/popups.
        Popup.initAll();

        // Close context menu/popups when clicked outside.
        document.addEventListener('click', function(e) {
            // Handle onEnter event in form input elements.
            if (!(e.target.nodeName == 'BUTTON' && e.pageX == 0)) {
                Popup.checkVisibility(e);
            }
        });

        // Handle ESC button and hide active popup.
        window.addEventListener('keyup', function(e) {
            if ( e.keyCode === 27 ) {
                Popup.hideActive();
            }
        });
    },

    /**
     * Load the tree in DOM.
     */
    loadTree: function() {
        if (this.readyState == 4) {
            var data = JSON.parse(this.responseText);
            if (data.error) {
                Popup.showError(data.error);
            } else {
                var node = new TreeNode(data, null);
                getById(Tree.containerId).appendChild(node.render());
                getById('tree-controls').removeClass('hidden');
            }
        }
    },

    /**
     * Send GET request to API.
     */
    doGet: function(uri, callback) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = callback;
        xhttp.open('GET', uri, true);
        xhttp.send();
    },

    /**
     * Send POST request to API.
     */
    doPost: function(data) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
                var data = JSON.parse(this.responseText);
                if (data.error) {
                    Popup.showFormErrors([data.error]);
                } else {
                    Popup.hideActive();
                    Tree.currentNode.reload(true);
                }
            }
        };
        xhttp.open('POST', this.currentNode.uri, true);
        xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhttp.send(JSON.stringify(data));
    },

    /**
     * Send PUT request to API using tunneling.
     */
    doPut: function(data) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
                var data = JSON.parse(this.responseText);
                if (data.error) {
                    Popup.showFormErrors([data.error]);
                } else {
                    Popup.hideActive();
                    if (Tree.currentNode.isRoot()) {
                        Tree.currentNode.reload();
                    } else {
                        Tree.currentNode.reloadParent();
                    }
                }
            }
        };
        xhttp.open('PUT', this.currentNode.uri, true);
        xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhttp.send(JSON.stringify(data));
    },

    /**
     * Send DELETE request to API using tunneling.
     */
    doDelete: function() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 204) {
                    Popup.hideActive();
                    Tree.currentNode.delete();
                    Tree.currentNode.reloadParent();
                } else {
                    var data = JSON.parse(this.responseText);
                    if (data.error) {
                        Popup.showFormErrors([data.error]);
                    }
                }
            }
        };
        xhttp.open('DELETE', this.currentNode.uri, true);
        xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhttp.send();
    },

    /**
     * Validate node item form data.
     */
    validateForm: function(form) {
        var title = form.getElementsByClassName('title')[0].value.trim(),
            sanitized = {
                title: '',
                errors: []
            };

        if (title.length == 0) {
            sanitized.errors.push(Tree.validation.titleEmptyError);
        } else if (title.length > Tree.validation.titleLength) {
            sanitized.errors.push(Tree.validation.titleTooLongError);
        } else {
            sanitized.title = title;
        }

        return sanitized;
    },

    /**
     * Fully expand the tree.
     */
    expand: function() {
        var nodes = getById(this.containerId).getElementsByClassName('item');
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i],
                nodeList = node.getElementsByTagName('ul')[0];
            if (nodeList) {
                node.removeClass('plus');
                node.addClass('minus');
                nodeList.removeClass('collapsed');
            }
        }
    },

    /**
     * Fully colapse the tree.
     */
    collapse: function() {
        var nodes = getById(this.containerId).getElementsByClassName('item');
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i],
                nodeList = node.getElementsByTagName('ul')[0];
            if (nodeList) {
                node.removeClass('minus');
                node.addClass('plus');
                nodeList.addClass('collapsed');
            }
        }
    },

    /**
     * Init expand/collapse tree buttons.
     */
    initControls: function() {
        document.getElementsByClassName('expand')[0].addEventListener('click', function() {
            Tree.expand();
        });
        document.getElementsByClassName('collapse')[0].addEventListener('click', function() {
            Tree.collapse();
        });
    }
};

////////////////////////////////////
////////// TreeNode
////////////////////////////////////
/**
 * Represent a tree node.
 */
function TreeNode(data, parent) {
    this.id = data.id;
    this.title = data.title;
    this.parentId = data.parentId;
    this.uri = data.uri;
    this.children = [];
    this.titleClass = 'title';
    this.parent = parent;

    var $this = this;
    data.children.forEach(function(data) {
        $this.children.push(new TreeNode(data, $this));
    });
}

/**
 * Check if node is root
 */
TreeNode.prototype.isRoot = function() {
    return this.parentId ?
        false :
        true;
};

/**
 * Render a tree node.
 */
TreeNode.prototype.render = function() {
    var li = document.createElement('li');
    li.setAttribute('id', 'node_' + this.id);
    if (this.children.length > 0) {
        li.setAttribute('class', 'item plus');
    } else {
        li.setAttribute('class', 'item empty');
    }

    // Node item title.
    var title = document.createElement('div');
    title.setAttribute('class', this.titleClass);
    title.innerHTML = this.title;

    // Show context menu when node item is clicked with mouse right button.
    var $this = this;
    title.addEventListener('contextmenu', function(e) {
        e.preventDefault();

        Tree.currentNode = $this;
        Popup.showContextMenu(e.pageX, e.pageY);

        return false;
    });
    // Toggle node item when clicked with mouse left button.
    title.addEventListener('click', function() {
        $this.toggle();
    });
    li.appendChild(title);

    if (this.children.length > 0) {
        var childrenNodeList = document.createElement('ul');
        childrenNodeList.setAttribute('class', 'collapsed');

        this.children.forEach(function(child) {
            childrenNodeList.appendChild(child.render());
        });

        li.appendChild(childrenNodeList);
    }

    return li;
};

/**
 * Expand node item.
 */
TreeNode.prototype.expand = function() {
    var nodeItem = getById('node_' + this.id),
        nodeList = nodeItem.getElementsByTagName('ul')[0];

    if (nodeList) {
        nodeItem.removeClass('plus');
        nodeItem.addClass('minus');
        nodeList.removeClass('collapsed');
    }
};

/**
 * Collapse node item.
 */
TreeNode.prototype.collapse = function() {
    var nodeItem = getById('node_' + this.id),
        nodeList = nodeItem.getElementsByTagName('ul')[0];

    if (nodeList) {
        nodeItem.removeClass('minus');
        nodeItem.addClass('plus');
        nodeList.addClass('collapsed');
    }
};

/**
 * Toggle node item.
 */
TreeNode.prototype.toggle = function() {
    var childrenNodeList = getById('node_' + this.id).getElementsByTagName('ul')[0];
    if (childrenNodeList) {
        if (childrenNodeList.hasClass('collapsed')) {
            this.expand();
        } else {
            this.collapse();
        }
    }
};

/**
 * Delete node item
 */
TreeNode.prototype.delete = function() {
    // Remove node from parent
    this.parent.children.splice(this.parent.children.indexOf(this), 1);

    // Remove node item from DOM
    var nodeLi = getById('node_' + Tree.currentNode.id),
        ul = nodeLi.parentNode;
    ul.removeChild(nodeLi);
};

/**
 * Get nodes, which are expanded.
 */
TreeNode.prototype.getExpandedSchema = function(expand) {
    var expandedNodes = [],
        search = function(node) {
            var nodeItem = getById('node_' + node.id);
            if (nodeItem) {
                var nodeList = nodeItem.getElementsByTagName('ul')[0];
                if (nodeList && !nodeList.hasClass('collapsed')) {
                    expandedNodes.push(node);
                }
                if (node.children.length > 0) {
                    node.children.forEach(function(child) {
                        search(child);
                    });
                }
            }
        };

    // Expand node if flag is set. Used when new item is created.
    var nodeList = getById('node_' + this.id).getElementsByTagName('ul')[0];
    if (!nodeList || (expand || nodeList.hasClass('collapsed'))) {
        expandedNodes.push(this);
    }

    // Search nodes recursively.
    search(this);

    return expandedNodes;
};

/**
 * Expand nods from schema.
 */
TreeNode.prototype.applyExpandedSchema = function(expandedNodes) {
    expandedNodes.forEach(function(node) {
        node.expand();
    });
};

/**
 * Reload node content.
 */
TreeNode.prototype.reload = function(expand) {
    var node = this;
    Tree.doGet(node.uri, function() {
        if (this.readyState == 4) {
            var data = JSON.parse(this.responseText);
            if (data.error) {
                Popup.showError(data.error);
            } else {
                // Get expanded nodes schema and update new node after rendered in DOM.
                var expanded = node.getExpandedSchema(expand),
                    newNode = new TreeNode(data, node.parent),
                    nodeItem = getById('node_' + newNode.id),
                    parentNodeList = nodeItem.parentNode;
                parentNodeList.replaceChild(newNode.render(), nodeItem);
                newNode.applyExpandedSchema(expanded);
                node = newNode;
            }
        }
    });
};

/**
 * Reload parend node content.
 */
TreeNode.prototype.reloadParent = function() {
    this.parent.reload();
};

////////////////////////////////////
////////// Popup
////////////////////////////////////
/**
 * Process tree node context menu and action(add/edit/delete node) popups.
 */
var Popup = {
    active: null,
    topLeftX: 0,
    topLeftY: 0,

    /**
     * Init all popups
     */
    initAll: function() {
        this.initItemContextMenuPopup();
        this.initAddPopup();
        this.initEditPopup();
        this.initDeletePopup();
        this.initErrorPopup();
    },

    /**
     * Init node item context menu.
     */
    initItemContextMenuPopup: function() {
        var popup = getById('item_context_menu_popup');
            this.item_context_menu_popup = popup;

        var $this = this;
        popup.getElementsByClassName('add_item')[0].addEventListener('click', function(e) {
            e.preventDefault();
            $this.showAdd();
        });
        popup.getElementsByClassName('edit_item')[0].addEventListener('click', function(e) {
            e.preventDefault();
            $this.showEdit();
        });
        popup.getElementsByClassName('delete_item')[0].addEventListener('click', function(e) {
            e.preventDefault();
            $this.showDelete();
        });
    },

    /**
     * Init popup for add new node item action.
     */
    initAddPopup: function() {
        var popup = getById('add_popup'),
            form = popup.getElementsByTagName('form')[0];
        this.add_popup = popup;

        this.initCancelButton(popup);
        var $this = this;
        form.addEventListener('submit', function(e) {
            e.preventDefault();

            var sanitized = Tree.validateForm(form);
            if (sanitized.errors.length > 0) {
                $this.showFormErrors(sanitized.errors);
                return false;
            }

            var data = {
                title: sanitized.title
            };
            Tree.doPost(data);
        });
    },

    /**
     * Init popup for edit node item action.
     */
    initEditPopup: function() {
        var popup = getById('edit_popup'),
            form = popup.getElementsByTagName('form')[0];
        this.edit_popup = popup;

        this.initCancelButton(popup);
        var $this = this;
        form.addEventListener('submit', function(e) {
            e.preventDefault();

            var sanitized = Tree.validateForm(form);
            if (sanitized.errors.length > 0) {
                $this.showFormErrors(sanitized.errors);
                return false;
            }

            var data = {
                id: Tree.currentNode.id,
                title: sanitized.title
            };
            Tree.doPut(data);
        });
    },

    /**
     * Init popup for delete node item action.
     */
    initDeletePopup: function() {
        var popup = getById('delete_popup');
        this.delete_popup = popup;

        this.initCancelButton(popup);
        popup.getElementsByClassName('delete')[0].addEventListener('click', function() {
            Tree.doDelete();
        });
    },

    /**
     * Init popup for errors not handled by other popups.
     */
    initErrorPopup: function() {
        var popup = getById('error_popup');
        this.error_popup = popup;

        this.initCancelButton(popup);
    },

    /**
     * Close popup on cancel button click.
     */
    initCancelButton: function(popup) {
        var $this = this;
        popup.getElementsByClassName('cancel')[0].addEventListener('click', function() {
            $this.hideActive();
        });
    },

    /**
     * Positon popup at the given coordinates.
     */
    setPosition: function(popup) {
        popup.style.left = this.topLeftX + 'px';
        popup.style.top  = this.topLeftY + 'px';
    },

    /**
     * Show popup.
     */
    show: function(popup) {
        this.hideActive();
        popup.addClass('active');
        this.active = popup;
    },

    /**
     * Show node item context menu.
     */
    showContextMenu: function(topLeftX, topLeftY) {
        var popup = this.item_context_menu_popup,
            deleteLink = popup.getElementsByClassName('delete_item')[0];

        // Set position coordinates
        this.topLeftX = topLeftX;
        this.topLeftY = topLeftY;

        if (Tree.currentNode.isRoot()) {
            deleteLink.addClass('hidden');
        } else {
            deleteLink.removeClass('hidden');
        }
        this.setPosition(popup);
        this.show(popup);
    },

    /**
     * Show popup for add new node item action.
     */
    showAdd: function() {
        var popup = this.add_popup;

        // Clear old content
        popup.getElementsByClassName('errors')[0].innerHTML = '';
        popup.getElementsByClassName('title')[0].value = '';

        this.setPosition(popup);
        this.show(popup);
    },

    /**
     * Show popup for edit node item action.
     */
    showEdit: function() {
        var popup = this.edit_popup;

        // Clear old content
        popup.getElementsByClassName('errors')[0].innerHTML = '';
        popup.getElementsByClassName('title')[0].value = decodeHtml(Tree.currentNode.title);

        this.setPosition(popup);
        this.show(popup);
    },

    /**
     * Show popup for delete node item action.
     */
    showDelete: function() {
        var popup = this.delete_popup;

        // Set node value in confirm message.
        popup.getElementsByClassName('placeholder')[0].innerHTML = Tree.currentNode.title;

        this.setPosition(popup);
        this.show(popup);
    },

    /**
     * Show popup for errors not handled by other popups.
     */
    showError: function(error) {
        var popup = this.error_popup;

        // Clear old content
        popup.getElementsByClassName('error')[0].innerHTML = error;
        this.show(popup);
    },

    /**
     * Show form errors in add/edit node item popup.
     */
    showFormErrors: function(errors) {
        var errorSlot = this.active.getElementsByClassName('errors')[0];

        // Clear old content
        errorSlot.innerHTML = '';

        for (var i = 0; i < errors.length; i++) {
            var error = document.createElement('li');
            error.textContent = errors[i];
            errorSlot.appendChild(error);
        }
    },

    /**
     * Check if clicked outside an active popup.
     */
    checkVisibility: function(e) {
        if (this.active) {
            var cursorX = e.pageX,
                cursorY = e.pageY,
                bottomRightX = this.topLeftX + this.active.offsetWidth,
                bottomRightY = this.topLeftY + this.active.offsetHeight;

            if ((cursorX < this.topLeftX || cursorX > bottomRightX) || (cursorY < this.topLeftY || cursorY > bottomRightY)) {
                this.hideActive();
            }
        }
    },

    /**
     * Hide active popup.
     */
    hideActive: function() {
        if (this.active) {
            this.active.removeClass('active');
            this.active = null;
        }
    }
};
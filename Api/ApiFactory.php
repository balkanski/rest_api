<?php

namespace Api;

use Exception\ApiException;
use Http\Http;
use Http\Request;
use PDO;

class ApiFactory
{
    /**
     * Mapping of the resource name and the proper api class to process it.
     */
    protected static $resources = array(
        'tree' => 'Api\TreeApi',
    );

    /**
     * @param Request $request
     * @param PDO     $pdo
     *
     * @return \Api\class
     *
     * @throws ApiException
     */
    public static function createApi(Request $request, PDO $pdo)
    {
        $class = @static::$resources[$request->getResourceName()];

        if (class_exists($class)) {
            return new $class($request, $pdo);
        } else {
            throw new ApiException('Invalid resource uri', Http::STATUS_NOT_FOUND);
        }
    }
}

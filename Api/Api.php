<?php

namespace Api;

use Exception\ApiException;
use Http\Http;
use Http\Request;
use PDO;

abstract class Api
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @param Request $request
     * @param PDO     $pdo
     */
    public function __construct(Request $request, PDO $pdo)
    {
        $this->request = $request;
        $this->pdo = $pdo;
    }

    /**
     * Main entry point.
     *
     * @throws ApiException
     */
    public function process()
    {
        $method = $this->getRequest()->getMethod();
        switch ($method) {
            case 'GET':
                return $this->doGet();

            case 'POST':
                return $this->doPost();

            case 'PUT':
                return $this->doPut();

            case 'DELETE':
                return $this->doDelete();

            default:
                throw new ApiException('Invalid method: '.$method, Http::STATUS_METHOD_NOT_ALLOWED);
        }
    }

    /**
     * Process GET request.
     */
    abstract protected function doGet();

    /**
     * Process POST request.
     */
    abstract protected function doPost();

    /**
     * Process PUT request.
     */
    abstract protected function doPut();

    /**
     * Process DELETE request.
     */
    abstract protected function doDelete();

    /**
     * Validates incoming data.
     *
     * @param mixed $data
     */
    abstract protected function validateData($data);

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @return PDO
     */
    protected function getPdo()
    {
        return $this->pdo;
    }
}

<?php

namespace Api;

trait ResourceUriTrait
{
    /**
     * The absolute uri of a resource.
     *
     * @var string
     */
    protected $uri;

    /**
     * Sets the resource uri.
     *
     * @param string $uri The absolute uri of a resource
     */
    public function setResourceUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * Gets the resource uri.
     *
     * @return string
     */
    public function getResourceUri()
    {
        return $this->uri;
    }
}

<?php

namespace Api;

interface Resource
{
    /**
     * Sets the absolute resource uri to a resource object.
     *
     * @param string
     */
    public function setResourceUri($uri);

    /**
     * Gets the resource uri from a resource object.
     *
     * @return string
     */
    public function getResourceUri();

    /**
     * Get a json representation of the resource object.
     *
     * @return string
     */
    public function toJson();
}

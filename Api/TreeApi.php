<?php

namespace Api;

use Exception\ApiException;
use Http\Http;
use Http\Request;
use Http\Response;
use Model\Node;
use Model\Tree;
use PDO;

class TreeApi extends Api
{
    const VALIDATION_ROOT_MSG = 'Root cannot be deleted!';
    const VALIDATION_TITLE_LENGTH = 50;
    const VALIDATION_TITLE_EMPTY_MSG = 'Value should not be empty.';
    const VALIDATION_TITLE_LONG_MSG  = 'Value should be less than 50 characters.!';

    /**
     * @var Tree
     */
    protected $tree;

    /**
     * @param Request $request
     * @param PDO     $pdo
     */
    public function __construct(Request $request, PDO $pdo)
    {
        parent::__construct($request, $pdo);

        $this->tree = new Tree($pdo, $this->getResourceUri());
    }

    /**
     * Process GET request.
     *
     * @return Response
     */
    protected function doGet()
    {
        $node = $this->getTree()->getNode($this->getResourceId());
        $node = $this->validateNode($node);

        return new Response($node->toJson(), Http::STATUS_OK);
    }

    /**
     * Process POST request.
     *
     * @return Response
     */
    protected function doPost()
    {
        $parentNode = $this->getTree()->getNode($this->getResourceId());
        $parentNode = $this->validateNode($parentNode);
        $data = $this->validateData($this->getData());
        $node = $this->getTree()->createNode($parentNode, $data);

        return new Response('', Http::STATUS_CREATED, $node->getResourceUri());
    }

    /**
     * Process PUT request.
     *
     * @return Response
     */
    protected function doPut()
    {
        $node = $this->getTree()->getNode($this->getResourceId());
        $node = $this->validateNode($node);
        $data = $this->validateData($this->getData());
        $node = $this->getTree()->updateNode($node, $data);

        return new Response($node->toJson(), Http::STATUS_OK);
    }

    /**
     * Process DELETE request.
     *
     * @return Response
     *
     * @throws ApiException
     */
    protected function doDelete()
    {
        $node = $this->getTree()->getNode($this->getResourceId());
        $this->validateNode($node);
        if ($node->isRoot()) {
            throw new ApiException(static::VALIDATION_ROOT_MSG, Http::STATUS_FORBIDDEN);
        }
        $this->getTree()->deleteNode($node);

        return new Response('', Http::STATUS_NO_CONTENT);
    }

    /**
     * Validates incoming request data.
     *
     * @param array $data
     *
     * @return array
     *
     * @throws ApiException
     */
    protected function validateData($data)
    {
        if (!is_array($data) || !array_key_exists('title', $data) || !trim($data['title'])) {
            throw new ApiException(static::VALIDATION_TITLE_EMPTY_MSG, Http::STATUS_UNPROCESSABLE_ENTITY);;;
        }
        if (mb_strlen($data['title']) > static::VALIDATION_TITLE_LENGTH) {
            throw new ApiException(static::VALIDATION_TITLE_LONG_MSG, Http::STATUS_UNPROCESSABLE_ENTITY);
        }

        return $data;
    }

    /**
     * @return string
     */
    protected function getResourceUri()
    {
        return $this->getRequest()->getResourceUri();
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return $this->getRequest()->getData();
    }

    /**
     * Validate node.
     *
     * @param Node $node
     *
     * @return Node
     *
     * @throws ApiException
     */
    protected function validateNode(Node $node = null)
    {
        if (!$node) {
            throw new ApiException('Invalid resource uri', Http::STATUS_NOT_FOUND);
        }

        return $node;
    }

    /**
     * @return Tree
     */
    protected function getTree()
    {
        return $this->tree;
    }

    /**
     * Get resource id from request params.
     *
     * @return int or null
     *
     * @throws ApiException
     */
    protected function getResourceId()
    {
        $id = $this->getRequest()->getResourceId();
        if (is_numeric($id) || !$id) {
            return $id;
        } else {
            throw new ApiException('Invalid resource uri', Http::STATUS_NOT_FOUND);
        }
    }
}

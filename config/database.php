<?php
/**
 * Database config params.
 */
$db_host = '127.0.0.1';
$db_name = 'rest_api';
$db_user = 'rest_api';
$db_pass = 'rest_api';
$db_charset = 'utf8mb4';

$dsn = "mysql:host=$db_host;dbname=$db_name;charset=$db_charset";
$pdo_opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];

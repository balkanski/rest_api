SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `tree`;
CREATE TABLE `tree` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `tree` (`id`, `title`, `parent_id`) VALUES
(1, 'Local Disc (C:)', NULL),
(2, 'Apache', 1),
(3, 'bin', 2),
(4, 'iconv', 3),
(5, 'conf', 2),
(6, 'extra', 5),
(7, 'htdocs', 2),
(8, 'Projects', 7),
(9, 'Program Files', 1),
(10, 'Git', 9),
(11, 'bin', 10),
(12, 'dev', 10),
(13, 'etc', 10),
(14, 'usr', 10),
(15, 'bin', 14),
(16, 'lib', 14),
(17, 'ssl', 14),
(18, 'Internet Explorer', 9),
(19, 'MySQL', 9),
(20, 'MySQL Server 5.6', 19),
(21, 'bin', 20),
(22, 'data', 20);


ALTER TABLE `tree`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tree_parent_id` (`parent_id`);


ALTER TABLE `tree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

ALTER TABLE `tree`
  ADD CONSTRAINT `tree_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `tree` (`id`) ON DELETE CASCADE;

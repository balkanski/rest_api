<?php

namespace Exception;

use Exception;

class ApiException extends Exception
{
    /**
     * Response status code, e.g. 200 OK.
     *
     * @var string
     */
    protected $statusCode;

    /**
     * @param string $message    The exception message
     * @param string $statusCode The response status code
     */
    public function __construct($message, $statusCode)
    {
        parent::__construct($message);

        $this->statusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}

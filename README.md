# A simple RESTful API implemented in pure PHP which handles the basic CRUD operations on a JavaScript tree menu.

The project requires PHP, MySQL and a web server installed, e.g. Apache(with mod_rewrite enabled).

## Installation

1. Clone the project into a suitable directory, which can be accessed by your web server, e.g. C:/Apache/htdocs/rest_api.
2. Create a MySQL database called 'rest_api'.
3. Add a new MySQL user to operate with the new database. Username - 'rest_api', password - 'rest_api'.
4. Alternatively to step 2 and 3, you can edit the configuration file and set another database connection parameters, see /config/database.php file.
5. Import the /config/tree.sql schema in the 'rest_api' database. This will create the 'tree' table and the menu data.
6. Open http://localhost/rest_api in your favourite browser.
7. You should see a tree menu. A left mouse button click on a node toggles it and a right mouse button click shows a context menu with add/edit/delete actions.

## API specification

The API retrieves data in JSON format. According to the installation as described above, the API will be available at http://localhost/rest_api/api/tree . The API supports the following methods:

##### GET

Retrieve data for the whole tree or only part of it, i.e. inner node. Example resource URIs:
- http://localhost/rest_api/api/tree
- http://localhost/rest_api/api/tree/10

Test the API with CURL:
- curl -i -X GET localhost://rest_api/api/tree

##### POST

Add a new node to the tree. The request should include the following header:
- Content-Type = application/json; charset=utf-8

Data should be in the following format:
- {"title":"New node title"}

Example resource URIs:
- http://localhost/rest_api/api/tree
- http://localhost/rest_api/api/tree/10

Test the API with CURL, e.g. add a new node at root level:
- curl -i -X POST -d '{"title":"New node title"}' http://localhost/rest_api/api/tree

##### PUT

Updates a tree node title. The request should include the following header:
- Content-Type = application/json; charset=utf-8

Data should be in the following format:
- {"title":"New node title"}

Example resource URIs:
- http://localhost/rest_api/api/tree
- http://localhost/rest_api/api/tree/10

Test the API with CURL, updates node with id 10:
- curl -i -X PUT -d '{"title":"New node title"}' http://localhost/rest_api/api/tree/10

The API also supports tunneling, i.e. PUT request masked as POST. To use this, the request should be send via POST with the following extra header included:
- X-HTTP-Method-Override = PUT

Test with CURL:
- curl -i -X POST -H "X-HTTP-Method-Override: PUT" -d '{"title":"New node title"}' http://localhost/rest_api/api/tree/10

##### DELETE

Deletes a node. The root node cannot be deleted.

Example resource URI:
- http://localhost/rest_api/api/tree/10

Test the API with CURL, deletes node with id 10:
- curl -i -X DELETE http://localhost/rest_api/api/tree/10

The API also supports tunneling, i.e. DELETE request masked as POST. To use this, the request should be send via POST with the following extra header included:
- X-HTTP-Method-Override = DELETE

Test with CURL:
- curl -i -X POST -H "X-HTTP-Method-Override: DELETE" http://localhost/rest_api/api/tree/10

